from django.contrib.auth.management.commands import createsuperuser


class Command(createsuperuser.Command):
    help = "Create an admin user."

    def execute(self, *args, **options):
        verbosity = options['verbosity']
        options['verbosity'] = 0  # Silence createsuperuser output.

        # Call the createsuperuser command, which does all the dirty work
        # for us, while also setting is_staff/is_admin.
        super().execute(*args, **options)

        # However, the admin shouldn't be a superuser, so unset that.
        mgr = self.UserModel._default_manager.db_manager(options['database'])
        user = mgr.last()  # User created in createsuperuser call.
        user.is_superuser = False
        user.save()

        if verbosity >= 1:
            self.stdout.write("Admin created successfully.")
