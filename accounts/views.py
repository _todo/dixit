from django.contrib.auth import login
from django.contrib.sites.shortcuts import get_current_site
from django.core.mail import EmailMessage
from django.shortcuts import redirect, render
from django.template.loader import render_to_string
from django.utils.encoding import force_bytes, force_text
from django.utils.http import urlsafe_base64_decode, urlsafe_base64_encode

from dixit.models import Player
from dixit.settings import SIGNUP_REDIRECT_URL
from .forms import SignUpForm
from .tokens import activation_token_generator


def signup(request):
    if request.user.is_authenticated:
        return redirect(SIGNUP_REDIRECT_URL)

    if request.method == 'POST':
        form = SignUpForm(request.POST)
        if form.is_valid():
            user = form.save(commit=False)
            user.is_active = False
            user.save()

            subject = 'Activate Your Dixit Account'
            message = render_to_string(
                'accounts/activation_email.html', {
                    'user': user,
                    'protocol': request.scheme,
                    'domain': get_current_site(request).domain,
                    'uid': urlsafe_base64_encode(force_bytes(user.pk)).decode(),
                    'token': activation_token_generator.make_token(user),
                })
            to_email = form.cleaned_data.get('email')
            EmailMessage(subject, message, to=[to_email]).send()

            return redirect('activation')

    return render(request, 'accounts/signup.html', {'form': SignUpForm()})


def activation(request):
    return render(request, 'accounts/activation.html')


def activation_confirm(request, uidb64, token):
    try:
        uid = force_text(urlsafe_base64_decode(uidb64))
        user = Player.objects.get(pk=uid)
    except (TypeError, ValueError, OverflowError, Player.DoesNotExist):
        user = None

    if user is not None and activation_token_generator.check_token(user, token):
        user.is_active = True
        user.email_confirmed = True
        user.save()
        login(request, user)
        return redirect('home')

    return render(request, 'accounts/activation_invalid.html')
