from django.contrib.auth import views as auth_views
from django.urls import path, re_path

from . import views


_uidb64 = r'(?P<uidb64>[0-9A-Za-z_\-]+)'
_token = r'(?P<token>[0-9A-Za-z]{1,13}-[0-9A-Za-z]{1,20})'

urlpatterns = [
    # Login and signup
    path('login/',
         auth_views.LoginView.as_view(
             template_name='accounts/login.html',
             redirect_authenticated_user=True),
         name='login'),
    path('logout/',
         auth_views.LogoutView.as_view(),
         name='logout'),
    path('signup/',
         views.signup,
          name='signup'),

    # Account activation
    path('activation/',
         views.activation,
         name='activation'),
    re_path(rf'^activation/{_uidb64}/{_token}/$',
            views.activation_confirm,
            name='activation_confirm'),

    # Password change
    path('password/',
         auth_views.PasswordChangeView.as_view(
             template_name='accounts/password_change.html'),
         name='password_change'),
    path('password/done/',
         auth_views.PasswordChangeDoneView.as_view(
             template_name='accounts/password_change_done.html'),
         name='password_change_done'),

    # Password reset
    path('password_reset/',
         auth_views.PasswordResetView.as_view(
             template_name='accounts/password_reset.html',
             email_template_name='accounts/password_reset_email.html',
             subject_template_name='accounts/password_reset_subject.txt'),
         name='password_reset'),
    path('password_reset/done/',
         auth_views.PasswordResetDoneView.as_view(
             template_name='accounts/password_reset_done.html'),
         name='password_reset_done'),
    re_path(rf'^password_reset/{_uidb64}/{_token}/$',
            auth_views.PasswordResetConfirmView.as_view(
                template_name='accounts/password_reset_confirm.html'),
            name='password_reset_confirm'),
    path('password_reset/complete/',
         auth_views.PasswordResetCompleteView.as_view(
             template_name='accounts/password_reset_complete.html'),
         name='password_reset_complete'),
]
