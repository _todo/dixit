import React from 'react'

import {ScoreboardTable} from "./styles";


export default class Scoreboard extends React.Component {
  constructor(props) {
    super(props);
  }


  render() {
    let len = 0;
    this.props.players.map(p => p.username.length>len?len=p.username.length:null)
    return (<ScoreboardTable>
      {/*<table style={{marginRight: "10px"}}>*/}
        {/*<thead>*/}
        {/*<tr>*/}
          {/*<th className={'username'}>&nbsp;</th>*/}
        {/*</tr>*/}
        {/*</thead>*/}
        {/*<tbody>*/}
        {/*{this.props.players.map(*/}
          {/*(player, i) =>*/}
            {/*<tr><td className={'username'}>*/}
              {/*{player.username}*/}
            {/*</td></tr>*/}
          {/*)*/}
        {/*}*/}
        {/*</tbody>*/}
      {/*</table>*/}

      <table style={{ width: "100%", minWidth: "800px", overflowX:"scroll", tableLayout:"fixed"}}>
        <thead>
        <tr>
          <th className={'username'} style={{width: len+"rem"}}>&nbsp;</th>
          {Array(30).fill(1).map(
            (x,i) =>
                <th className={'cell headerCell'}>{i+1}</th>
          )}
        </tr>
        </thead>
        <tbody>
        {this.props.players.map(
          (player, i) => <tr>
            <td className={'username'}>
              {player.username}
            </td>
            {Array(30).fill(1).map((x,j) =>
              {
                if (this.props.points && this.props.points[player.id])
                  return (j+1 > this.props.points[player.id].score-this.props.points[player.id].streak && j+1<this.props.points[player.id].score)?
                    <td className={'cell'}>{' '}</td>:
                    j+1===this.props.points[player.id].score?
                      <td className={'cell'}><img src={player.avatar.image} style={{maxWidth:"100%"}}/></td>:
                      <td className={'cell'}>{' '}</td>
                else
                  return <td>' '</td>
              }
            )}
          </tr>)}
        </tbody>
      </table>

    </ScoreboardTable>)
  }

};
