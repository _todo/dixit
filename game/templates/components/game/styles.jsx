import styled from 'styled-components';

export const MainLayout = styled.div`
  width: 100%;
  
  #keyword_inp {
    background-color: rgba(0,0,0,0);
    border: 3px solid black;
    border-radius: 4px;
    font-size: 2rem;
    text-align: center;
        margin-bottom: 4vh;
    border-top: none;
    border-radius: 0 0 4px 4px;
  }
  
  #keyword_inp:disabled {
    background-color: rgba(189, 189, 189, 0.1);
  }
  
 .glowbutton {
    background-color: #007bff;
    -webkit-border-radius: 10px;
    font-size: 1rem;
    line-height: 1.5;
    border-radius: .25rem;
    border: none;
    color: #FFFFFF;
    cursor: pointer;
    display: inline-block;
    font-family: Arial;
    padding: .375rem .75rem;  text-align: center;
    text-decoration: none;
    -webkit-animation: glowing 1500ms infinite;
    -moz-animation: glowing 1500ms infinite;
    -o-animation: glowing 1500ms infinite;
    animation: glowing 1500ms infinite;
  }
@-webkit-keyframes glowing {
  0% { background-color: #007bff; -webkit-box-shadow: 0 0 9px #007bff; }
  50% { background-color: #54a6ff; -webkit-box-shadow: 0 0 18px #54a6ff; }
  100% { background-color: #007bff; -webkit-box-shadow: 0 0 9px #007bff; }
}

@-moz-keyframes glowing {
  0% { background-color: #007bff; -moz-box-shadow: 0 0 9px #007bff; }
  50% { background-color: #54a6ff; -moz-box-shadow: 0 0 18px #54a6ff; }
  100% { background-color: #007bff; -moz-box-shadow: 0 0 9px #007bff; }
}

@-o-keyframes glowing {
  0% { background-color: #007bff; box-shadow: 0 0 9px #007bff; }
  50% { background-color: #54a6ff; box-shadow: 0 0 18px #54a6ff; }
  100% { background-color: #007bff; box-shadow: 0 0 9px #007bff; }
}

@keyframes glowing {
  0% { background-color: #007bff; box-shadow: 0 0 9px #007bff; }
  50% { background-color: #54a6ff; box-shadow: 0 0 8px #54a6ff; }
  100% { background-color: #007bff; box-shadow: 0 0 9px #007bff; }
}
}
`;

export const TableLayout = styled.div`
  text-align: center;
  align-content: center;
  justify-content: center;
  height: auto;
  padding: 2vh 0;
    
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
  
  img {
    max-width: 12vw;
    max-height: 30vh;
    padding: 10px;
    transition: all .2s ease-in-out;
    border-radius: 20px;
  }
  
  img:hover {
    transform: scale(1.3);
  }
  
  .grow {
    transform: scale(1.3);
  }
`;

export const WinnersLayout = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr;
  margin: 10px;
  width:30%
  * {
    margin: 5px;
  }
  
  img {
    width: 100px;
  }
 
  
  .name {
    font-weight: bold;
  }

`;

export const UserLayout = styled.div`
  img {
      width: 100px;
  }

  .name {
    font-weight: bold;
  }
`

export const PlayersLayout = styled.div`
  display: grid;
  grid-template-rows: 1fr 1fr 1fr 1fr 1fr 1fr;
  margin: 10px;
  background: lightblue;
  
  * {
    margin: 5px;
  }
  
  img {
    width: 100px;
  }
  
  .user {
        border: 1px solid grey;
    border-radius: 2px;
  }
  
  .name {
    font-weight: bold;
  }
`;

export const HandGrid = styled.div`
  display: grid;
  grid-template-columns: 1fr 1fr 1fr 1fr 1fr 1fr;
  
  img {
    max-width: 12vw;
    max-height: 30vh;
    padding: 10px;
    transition: all .2s ease-in-out;
    border-radius: 20px;
  }
  
  img:hover {
    transform: scale(1.3);
  }
  .grow {
    transform: scale(1.3);
  }
`;

export const StatusDiv = styled.div`
  text-align: center;
  border: 3px solid black;
  border-radius: 4px;
  font-size: 2rem;
`;


export const ScoreboardTable = styled.div`
  color: #4c555e;
  display: inline-flex;
  text-align: center;
  
  th,td {
    max-width: 50px;
    max-height: 50px;
    padding: 3px;
  }
  
  .cell {
    border-left: 2px solid grey;
    border-right: 2px solid grey;
  }
  
  .username {
  }
  
  .streakCell {
    background: #6a7b8c;
  }
  
`;
