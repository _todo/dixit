import React from 'react';

import Websocket from 'react-websocket';

import MyHand from './MyHand';
import Table from './Table';
import StatusBar from './StatusBar';
import Scoreboard from './Scoreboard';
import Players from './Players.jsx'

import {MainLayout, StatusDiv, UserLayout, WinnersLayout} from './styles';

import {game_participants, get_participants, render_component} from './index';


export default class Game extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      game: {},
      displayStatus: null,
      myHand: [],
      currentlyPicked: null,
      voted: null,
      defaultCard: "/media/expansions/default.png",
    };

    this.onClickSubmitCard = this.onClickSubmitCard.bind(this);
    this.onClickPickMyCard = this.onClickPickMyCard.bind(this);
    this.handleData = this.handleData.bind(this);
    this.sendSocketMessage = this.sendSocketMessage.bind(this);
    this.getHand = this.getHand.bind(this);
    this.myTurn = this.myTurn.bind(this);
    this.onClickVoteCard = this.onClickVoteCard.bind(this);
    this.displayAddedPoints = this.displayAddedPoints.bind(this);
    this.continueGame = this.continueGame.bind(this);
    this.endGameDisplay = this.endGameDisplay.bind(this);
  }

  componentDidMount() {
  }

  endGameDisplay() {
    const style = {    width: "50%", minWidth: "400px", margin: "auto", marginTop: "40px"}

    return(
        this.state.game.details.winners.length>1?
          <StatusDiv style={style}>
            <h1>Winners</h1>
            <WinnersLayout>
            {this.state.game.details.winners.map(
              player => {
                let p = game_participants.find(e=>e.id===player.id)
                if (p) return (<span><img src={p.profile_picture} alt="profile picture"/>
                  <div className="name">{p.username}</div></span>)
              }
            )}
            </WinnersLayout>
          </StatusDiv>

          :

          <StatusDiv style={style}>
            <h1>Winner</h1>
            <UserLayout>
              {this.state.game.details.winners.map(
                player => {
                  let p = game_participants.find(e=>e.id===player.id);
                  console.log(p)
                  if (p) return (<span><img src={p.profile_picture} alt="profile picture"/>
                  <div className="name">{p.username}</div></span>)
                }
              )}
            </UserLayout>
          </StatusDiv>


    )
  }

  displayAddedPoints() {
    let message = "Added points: ";
    if (this.state.game && this.state.game.details.addedPoints)
      this.state.game.details.addedPoints.map((p, i) => {
          i !== 0 ? message += ", " : "";
          message += game_participants[i].username + ": +" + p
        }
      );
    return message;
  }

  continueGame() {
    this.sendSocketMessage({
      action: "CONTINUE_GAME",
      player_id: this.props.current_user,
    });
  }

  onClickVoteCard(props) {
    if (this.state.voted !== null) {
      alert("You already voted!");
      return;
    }
    if (props === this.state.currentlyPicked) {
      alert("You can't vote for your own card!");
      return;
    }
    this.setState({
      voted: props,
      displayStatus: "Waiting for others..."
    });
    this.sendSocketMessage({
      action: "VOTE_FOR_CARD",
      player_id: this.props.current_user,
      card: props
    });
  }

  onClickPickMyCard(props) {
    if (document.getElementById('keyword_inp').value.length === 0) {
      alert("Keyword can't be blank!");
      return;
    }
    this.sendSocketMessage({
      action: "PICK_CARD_ON_TURN",
      player_id: this.props.current_user,
      keyword: document.getElementById('keyword_inp').value,
      card: props
    });
    document.getElementById('keyword_inp').value = '';
    this.setState({
      currentlyPicked: props,
      displayStatus: "Waiting for others..."
    });
  }

  onClickSubmitCard(props) {
    this.setState({
      currentlyPicked: props,
      displayStatus: "Waiting for others..."
    });
    this.sendSocketMessage({
      action: "PICK_CARD_ON_SUBMISSION",
      player_id: this.props.current_user,
      card: props
    });
  }

  getHand() {
    fetch('/game/hand/' + this.props.game_id + '/' + this.props.current_user + '/?format=json')
      .then(response => response.json()
      ).then(
      cards => {
        console.log(cards)
        this.setState({
          myHand: cards.cards.map(card => JSON.parse(card)),
          voted: cards.voted,
          currentlyPicked: cards.submitted
        });
        render_component()
      }
    );
  }

  myTurn() {
    if (!this.state.game.details) return false;
    return game_participants[this.state.game.details.turn].id === parseInt(this.props.current_user)
  }

  handleData(data) {
    let result = JSON.parse(data);
    console.log(result);

    this.setState({game: result.message});
    if (this.state.myHand.length === 0) this.getHand();
    render_component();
    switch (result.message.stage) {
      case 'WAITING_FOR_START':
        this.setState({displayStatus: "Waiting for other players to join..."})
        get_participants();
        break;
      case 'WAITING_FOR_PLAYER':
        get_participants();
        this.getHand();
        this.setState({
          displayStatus:
            this.myTurn() ? "Enter your keyword and pick a card" :
              "Waiting for " + game_participants[this.state.game.details.turn].username + " to make a move"
        });
        break;
      case 'WAITING_FOR_SUBMISSIONS':
        this.setState({
          displayStatus: this.myTurn() || this.state.currentlyPicked ? 'Waiting for other players to make a move...' : `Pick your card`
        });
        document.getElementById('keyword_inp').value = this.state.game.details.keyword;
        return;
      case 'WAITING_FOR_VOTES':
        this.setState({
          displayStatus: this.state.voted || this.myTurn() ? "Waiting for other players to vote" :
            "Vote!"
        });
        return;
      case 'TURN_RESULTS_OVERVIEW':
        document.getElementById('keyword_inp').value = '';
        this.setState({
          displayStatus: this.displayAddedPoints()
        });
        return;
      case 'GAME_OVER':
        this.setState({
          displayStatus: "Game over!"
        })
      default:
        return;
    }
  }

  sendSocketMessage(message) {
    const socket = this.refs.socket;
    socket.state.ws.send(JSON.stringify(message));
  }

  render() {
    console.log(this.state);
    return (
      <MainLayout>
        <Scoreboard
          points={this.state.game.points}
          players={game_participants}
        />
        <div style={{display: "flex"}}>
          <Players participants={game_participants}
                   current_player={this.props.current_user}
                   points={this.state.game.points}
          />
          <div style={{width: "100%"}}>
            {this.state.game.stage==='GAME_OVER'?
              this.endGameDisplay():
              <Table gameStatus={this.state.game.stage}
                     cards={this.state.game.details ? this.state.game.details.cards : null}
                     onClickPickedCard={this.onClickVoteCard}
                     defaultCard={this.state.defaultCard}
                     myTurn={this.myTurn}
                     submittedCount={this.state.game.details ? this.state.game.details.submittedCount : 0}
                     currentlyPicked={this.state.voted}
                     cardPairs={this.state.game.details ? this.state.game.details.cardPairs: null}
                     votes={this.state.game.details? this.state.game.details.votes : null}
                     players={game_participants}
              />
            }
              <div style={{height: "3rem", textAlign: "center"}}>
                <button
                  style={{
                    display: this.myTurn() && this.state.game.stage === 'TURN_RESULTS_OVERVIEW' ? "" : "none",
                  }}
                  className={'glowbutton'}
                  onClick={this.continueGame}
                >
                  Press to continue the game
                </button>
              </div>
              <StatusBar gameStatus={this.state.displayStatus}
              />
              <div style={{textAlign: 'center'}}>
                <input
                  disabled={(!this.myTurn() || this.state.game.stage !== 'WAITING_FOR_PLAYER')}
                  id={'keyword_inp'}
                  placeholder={'Keyword'}
                  name={'keyword_inp'}/>
              </div>
            <MyHand cards={this.state.myHand}
                    defaultCard={this.state.defaultCard}
                    onClickPickedCard={this.onClickPickMyCard}
                    onClickSubmitCard={this.onClickSubmitCard}
                    gameStatus={this.state.game.stage}
                    myTurn={this.myTurn}
                    currentlyPicked={this.state.currentlyPicked}
            />
            <Websocket ref="socket" url={this.props.socket}
                       onMessage={this.handleData}
                       onOpen={() => this.sendSocketMessage("joined")}
                       reconnect={true}/>
          </div>
        </div>
      </MainLayout>
    );
  }

}
