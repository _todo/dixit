import React from 'react'
import {PlayersLayout} from './styles'


//TODO get participants api
export default class Players extends React.Component {
  constructor(props) {
    super(props);
    this.state = { participants: []}
  }

  // componentWillMount(){
  //   fetch('http://127.0.0.1:8000/game/participants/2/?format=json').then(
  //     response => response.json()
  //   ).then(
  //     participants => {
  //       this.setState({participants})
  //     }
  //   )
  // }

  render() {
    let content = <PlayersLayout>
      {
        this.props.participants ?
        this.props.participants.map(
          (player, i) =>
            <div className="user">
              <img src={player.profile_picture} alt="profile picture"/>
              <div className="name">{player.username}</div>
              {this.props.points? <div>Answer streak: {this.props.points[player.id].streak}</div>:null}
            </div>
        ):null
      }
    </PlayersLayout>;
    return content;
  }

}
