import React from 'react';
import Game from './Game.jsx'
import ReactDOM from 'react-dom'

let current_user = null;
export let game_participants = null;
var game_id = document.getElementById("game_component").dataset.game_id;
var user_id = document.getElementById("players_component").dataset.user_id;

export function get_participants() {

    fetch(`http://localhost:8000/game/participants/${game_id}/${user_id}/?format=json`)
      .then(
      response => response.json()
    ).then(
      participants => {
        console.log(participants);
        game_participants = participants;
        render_component()
      }
    )
  // )
}


const game_sock = 'ws://' + window.location.host + "/ws/game/" + game_id + "/";
// preset the current_user

// renders out the base component
export function render_component() {
  ReactDOM.render(<Game current_user={user_id} socket={game_sock}
                        game_id={game_id}/>, document.getElementById('game_component'))
}

get_participants();
