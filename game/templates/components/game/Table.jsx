import React from 'react';
import {TableLayout} from './styles'
import {game_participants} from "./index";

export default class Table extends React.Component {
  render() {
    let content;
    switch (this.props.gameStatus) {
      case "WAITING_FOR_SUBMISSIONS":
        content = <TableLayout>
          {Array(this.props.submittedCount).fill(1).map(
            i => <img src={this.props.defaultCard}
                      alt="default"
            />
          )}
        </TableLayout>;
        break;
      case "WAITING_FOR_VOTES":
        content = <TableLayout>
          {
            this.props.cards.map(
              i => <img src={i.img}
                        onClick={() => this.props.currentlyPicked || this.props.myTurn() ? null : this.props.onClickPickedCard(i.id)}
                        alt={i.codename}
                        className={i.id === this.props.currentlyPicked ? "grow" : ""}
              />
            )
          }</TableLayout>;
        break;
      case "WAITING_FOR_RESULTS":
        content = <TableLayout>
          {
            this.props.cards.map(
              i => <img src={i.img}
                        alt={i.codename}
                        className={i.id === this.props.currentlyPicked ? "grow" : ""}
              />
            )
          }</TableLayout>;
        break;
      case "TURN_RESULTS_OVERVIEW":
        content = <TableLayout>
          {
            this.props.cards ?
              this.props.cards.map(
                i => {
                  let user = this.props.cardPairs ? parseInt(this.props.cardPairs[i.id]) : 0;
                  let user_name = this.props.players ? this.props.players.find(e => e.id === user).username : null;
                  let found = false;
                  return (
                    <div>

                      {user_name ? <div style={{marginBottom: "30px"}}>{user_name + "'s card"}<br/></div> : null}

                      <img src={i.img}
                           alt={i.codename}
                           className={i.id === this.props.currentlyPicked ? "grow" : ""}
                      />
                      <div style={{marginTop: "30px"}}>
                        {
                          damnit(this.props.votes, this.props.players, i.id)
                        }
                      </div>

                    </div>)
                }) : null
          }
        </TableLayout>
        ;
        break;
      default:
        content =
          <TableLayout> <img src={this.props.defaultCard} style={{display: "none"}}/></TableLayout>
    }
    return content
  }
}


const damnit = (votes, players, id) => {
  let text = '';
  let found = false;
  console.log(votes);
  if (votes)
    for (let key in votes) {
      if (votes[key] === id) {
        if (!found) {
          found = true;
          text += "Voted by:\n"
        }
        text += players.find(e => e.id === parseInt(key)) ? players.find(e => parseInt(e.id) === parseInt(key)).username : ''
      }
    }
  return text;
};
