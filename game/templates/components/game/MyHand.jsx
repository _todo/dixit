import React from 'react';
import {HandGrid} from './styles';

export default class MyHand extends React.Component {
  render() {
    let content = <div> </div>;
    if (this.props.cards===undefined) return content;
    switch(this.props.gameStatus){
      case "WAITING_FOR_PLAYER":
        content =  this.props.cards.map(
            i => <img src={i.img}
                      onClick={() => this.props.myTurn()?this.props.onClickPickedCard(i.id):null}
                      alt={i.codename}
                      className={i.id===this.props.currentlyPicked?"grow":""}

            />
          );
        break;
      case "WAITING_FOR_SUBMISSIONS":
        content =  this.props.cards.map(
          i => <img src={i.img}
                    onClick={() => this.props.onClickSubmitCard(i.id)}
                    className={i.id===this.props.currentlyPicked?"grow":""}
                    alt={i.codename}
          />
        );
        break;
      case "PICK_FROM_TABLE":
      case "WAITING_FOR_VOTES":
      case "WAITING_FOR_RESULTS":
        content = this.props.cards.map(
              i => <img src={ i.img }
                        alt={'default'}
                        className={i.id===this.props.currentlyPicked?"grow":""}
              />
            );
        break;
      case "GAME_OVER":
        content = this.props.cards.map(
          i => <img src={ this.props.defaultCard }
                    alt={'default'}
          />
        );
        break;
      default:
        content = this.props.cards.map(
              i => <img src={i.img}
                        alt={i.codename}
                        className={i.id===this.props.currentlyPicked?"grow":""}
              />
            );
    }
    return (
      <HandGrid>
        {content}
      </HandGrid>)
  }
}
