from asgiref.sync import async_to_sync
from channels.generic.websocket import WebsocketConsumer
import json
import redis
import pickle
from dixit.settings import REDIS_PORT, REDIS_HOST

class GameConsumer(WebsocketConsumer):
    def connect(self):
        self.game_id = self.scope['url_route']['kwargs']['game_id']
        self.game_group = 'game_%s' % self.game_id
        async_to_sync(self.channel_layer.group_add)(
            self.game_group,
            self.channel_name
        )

        self.accept()

    def disconnect(self, close_code):
        async_to_sync(self.channel_layer.group_discard)(
            self.game_group,
            self.channel_name
        )
        pass

    def receive(self, text_data):
        message = json.loads(text_data)

        print(message)

        if(type(message) is dict and 'action' in message):
            redis_db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)
            game = pickle.loads(redis_db.get(self.game_id))

            print ([*message])
            if (message['action']=='PICK_CARD_ON_TURN'):
                game.make_move(message['player_id'], message['card'], message['keyword'])

            elif (message['action']=='PICK_CARD_ON_SUBMISSION'):
                game.make_move_others(message['player_id'], message['card'])

            elif (message['action']=='VOTE_FOR_CARD'):
                game.vote(message['player_id'], message['card'])

            elif (message['action']=='CONTINUE_GAME'):
                game.continueGame(message['player_id'])

            redis_db.set(game.game_id, pickle.dumps(game))

        async_to_sync(self.channel_layer.group_send)(
            self.game_group,
            {
                'type' : 'game_update'
            }
        )

    def game_update(self, event):

        redis_db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)
        game = pickle.loads(redis_db.get(self.game_id))

        self.send(text_data=json.dumps({
            'type' : 'game_update',
            'message': game.get_payload()
        }))
