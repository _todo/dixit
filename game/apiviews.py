from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status
from dixit.serializers import PlayerSerializer
from dixit.models import Player
import redis
import pickle
import json
from dixit.settings import REDIS_HOST, REDIS_PORT

class CurrentUserView(APIView):

    def get(self, request):
        serializer = PlayerSerializer(request.user)
        return Response(serializer.data)

class GameParticipantsView(APIView):

    def get(self, request, *args, **kwargs):
        redis_db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)
        game =redis_db.get(kwargs['game_id'])
        user = Player.objects.get(id=kwargs['player_id'])

        if game is not None:
            game =  pickle.loads(game)
            participants = game.get_participants()

            found = False
            for p in participants:
                print(p,user)
                if p==user:
                    found=True
                    break

            if not found:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            results = [PlayerSerializer(participant, context={'request': request}).data for participant in participants]
            return Response(results)
        else:
            return Response(status=status.HTTP_404_NOT_FOUND)

class MyHandView(APIView):

    def get(self, request, *args, **kwargs):
        redis_db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)
        game =redis_db.get(kwargs['game_id'])
        if game is not None:
            game =  pickle.loads(game)
            hand = game.get_hand_for_user(kwargs['player_id'])
            if hand is None:
                return Response(status.HTTP_404_NOT_FOUND)
            for card in hand:
                if 'img' in card:
                    card['img'] = request.build_absolute_uri(card['img'])
            voted = game.get_voted(kwargs['player_id'])
            submitted = game.get_submitted(kwargs['player_id'])

            if hand is None:
                return Response(status=status.HTTP_401_UNAUTHORIZED)
            results = [json.dumps(card) for card in hand]
            ret = {
                "cards": results,
                "voted": voted,
                "submitted": submitted
            }
            return Response(ret)

        else:
            return Response(status.HTTP_404_NOT_FOUND)
