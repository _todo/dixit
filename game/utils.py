import redis
from dixit.settings import REDIS_HOST, REDIS_PORT
from dixit.models import AchievementType

def get_game_id():
    redis_db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)
    id_entry = redis_db.get("game_id_cnt")
    if id_entry is None:
        id_entry = 1
    id_entry = int(id_entry)
    redis_db.set("game_id_cnt", id_entry+1)
    return id_entry

def update_achievement(player, codename, func, def_value=0.0):
    defaults = {
        'player': player,
        'achievement_type': AchievementType.objects.get(codename=codename),
        'value': def_value,
    }

    a, _ = player.achievements.get_or_create(
        achievement_type__codename=codename, defaults=defaults
    )
    a.value = func(a.value)
    a.save()
