from django.urls import path

from . import apiviews, views


urlpatterns = [
    path('game/<int:game_id>/', views.GameView.as_view(), name='game'),
    path('create_game/',        views.create_game, name='create_game'),
    path('join_game/', views.join_game, name='join_game'),
    path('current-user/',
         apiviews.CurrentUserView.as_view()),
    path('game/participants/<int:game_id>/<int:player_id>/',
         apiviews.GameParticipantsView.as_view()),
    path('game/hand/<int:game_id>/<int:player_id>/',
         apiviews.MyHandView.as_view()),
]
