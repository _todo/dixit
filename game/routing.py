from django.urls import path

from . import consumers


websocket_urlpatterns = [
    path('ws/game/<game_id>/', consumers.GameConsumer),
]
