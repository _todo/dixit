import random

from django.http import  HttpResponse
import pickle
import redis

from asgiref.sync import async_to_sync
from channels.layers import get_channel_layer
from django.contrib import messages
from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render
from django.utils.decorators import method_decorator
from django.views.generic import TemplateView

from dixit.settings import REDIS_HOST, REDIS_PORT
from .forms import CreateGameForm
from .game import Game

from .utils import get_game_id

@login_required()
def create_game(request):
    if request.method == 'POST':
        form = CreateGameForm(request.POST)
        if form.is_valid():
            user = get_user(request)
            game_id = get_game_id()
            game = Game(game_id, user.id, int(form.cleaned_data['num_of_players']), str(form.cleaned_data['room_name']), user.username)
            redis_db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)

            games = {}
            if (redis_db.get('available_games') is not None):
                games = pickle.loads(redis_db.get('available_games'))
            games[str(game.game_id)] = {'free_places': game.player_limit - 1, 'limit': game.player_limit}

            redis_db.set('available_games', pickle.dumps(games))
            redis_db.set(game.game_id, pickle.dumps(game))
            return redirect('/game/' + str(game.game_id))
    form = CreateGameForm()
    return render(request, 'game/create_game.html', {'form': form})


class GameJoin:
    def __init__(self, name, current_players, players_limit, creator, id):
        self.name = name
        self.current_players = current_players
        self.players_limit = players_limit
        self.creator = creator
        self.id = id


def parse_to_table_data(arr):

    ret = ''
    for game in arr:
        ret += '<tr>'

        ret += '<td>'
        ret += game.name
        ret += '</td>'

        ret += '<td>'
        ret += str(game.current_players) + '/' + str(game.players_limit)
        ret += '</td>'

        ret += '<td>'
        ret += game.creator
        ret += '</td>'

        ret += '<td>'
        ret += '<a class="button" href="/game/' + str(game.id) + '">Join</a>'
        ret += '</td>'

        ret += '</tr>'

    return ret


@login_required()
def join_game(request):
    redis_db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)
    games = {}
    if redis_db.get('available_games') is not None:
        games = pickle.loads(redis_db.get('available_games'))
    arr = []

    for key, value in games.items():
        game_id = int(key)
        free_places = value['free_places']
        limit = value['limit']
        taken_places = limit - free_places
        game = pickle.loads(redis_db.get(game_id))
        arr.append(GameJoin(game.name, taken_places, limit, game.creator_name, game_id))

    if request.is_ajax():
        return HttpResponse(parse_to_table_data(arr))

    return render(request, 'game/join_game.html', {'arr': arr})

class GameView(TemplateView):
    template_name = 'game/index.html'
    game = None
    game_id = None
    user_id = None

    @method_decorator(login_required)
    def dispatch(self, request, *args, **kwargs):
        redis_db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)
        game_cache = redis_db.get(kwargs['game_id'])
        if not game_cache:
            messages.add_message(request, messages.ERROR, "soz")
            return redirect ('/join_game/')
        self.game_id = game_id = kwargs['game_id']

        # with cache._lock(game_id):
        user = get_user(request)
        self.user_id = user.id
        print("user", user)
        game = None


        self.game = game = pickle.loads(game_cache)
        if game.already_joined(user.id):
            return super(GameView, self).dispatch(request, *args, **kwargs)
        print("available", game.is_available())
        if game.is_available():
            success = game.add_player(user.id)
            print(success)
            if not success:
                messages.add_message(request, messages.ERROR, "nesto nece")
                return redirect('/join_game/')

            # with cache.lock('available_games'):
            games = pickle.loads(redis_db.get('available_games'))
            print(games)
            if game.has_started:
                games.pop(str(game_id))
            else:
                games[str(game_id)]['free_places'] -= 1

            print('game_%s' % self.game_id)
            channel_layer = get_channel_layer()
            async_to_sync(channel_layer.group_send)(
                'game_%s' % self.game_id,
                {"type": "game_update"}
            )

            redis_db.set('available_games', pickle.dumps(games))
            redis_db.set(game_id, pickle.dumps(game))

            return super(GameView, self).dispatch(request, *args, **kwargs)
        else:
            return redirect('/join_game/')


    def get_context_data(self, **kwargs):
        context = super(GameView, self).get_context_data(**kwargs)
        redis_db = redis.StrictRedis(host=REDIS_HOST, port=REDIS_PORT, db=0)

        if self.game_id is not None:
            game = redis_db.get(self.game_id)
            context['game'] = pickle.loads(game)
        return context
