import random
from dixit.models import Player, Card, Achievement, AchievementType
from channels.layers import get_channel_layer
import json
from asgiref.sync import async_to_sync
from .utils import update_achievement
import sys


CARD_COUNT = 84

class Game:
    guid = 1

    def __init__(self, game_id, creator_id, player_limit, name, creator_name):
        self.game_id = game_id
        self.creator_id = creator_id
        self.players = [{"id": creator_id, "score": 1, "streak": 0, "longestStreak" :0, "hand": []}]
        self.player_limit = player_limit
        tmp = list(range(1, CARD_COUNT))
        random.shuffle(tmp)
        self.deck = []
        for i in range(0, CARD_COUNT-1):
            self.deck.append(tmp[i])
        self.has_started = False
        self.has_ended = False
        self.player_turn = 0
        self.turn_details = {
            'stage': 'WAITING_FOR_START',
            'keyword': '',
            'right-card': -1,
            'card-pairs': dict(),
            'votes': dict(),
            'submitted': list(),
            'added-points': dict(),
            'winners' : list()
        }
        self.name = name
        self.creator_name = creator_name
        self.turn_count = 1

    def already_joined(self, player_id):
        for player in self.players:
            if player['id'] == player_id:
                return True
        return False

    def add_player(self, player_id):
        if self.turn_details['stage'] == 'WAITING_FOR_START':
            if (len(self.players) != self.player_limit):
                self.players.append({"id": player_id, "score": 1, "streak": 0, "longestStreak":0, "hand": []})
                if len(self.players) == self.player_limit:
                    self.start_game()
                return True
        return False

    def get_participants(self):
        participants = []
        for player in self.players:
            participants.append(Player.objects.get(id=player['id']))
        return participants

    def start_game(self):
        self.has_started = True
        self.init_hands()
        self.turn_details['stage'] = 'WAITING_FOR_PLAYER'
        channel_layer = get_channel_layer()
        async_to_sync(channel_layer.group_send)(
            f'game_{self.game_id}',
            {
                "type": "game_update",
                "message": json.dumps(self.get_payload())
            }
        )

    def init_hands(self):
        for player in self.players:
            player['hand']=[]
            for i in range(0,6):
                card = { 'id' : self.deck.pop()}
                card['img']=Card.objects.get(id=card['id']).image.url
                player['hand'].append(card)

    def get_hand_for_user(self, user_id):
        for player in self.players:
            if player['id'] == int(user_id):
                return player['hand']
        return None

    def get_voted(self, user_id):
        if str(user_id) in self.turn_details['votes']:
            return self.turn_details['votes'][str(user_id)]
        return None

    def get_submitted(self, user_id):
        if user_id == self.player_turn:
            if self.turn_details['right-card']!= -1:
                return int(self.turn_details['right-card'])
            return None
        for card, id in self.turn_details['card-pairs'].items():
            if user_id == int(id):
                return card
        return None

    def is_available(self):
        return (self.turn_details['stage'] == 'WAITING_FOR_START') and self.player_limit > len(self.players)

    def make_move(self, player_id, card, keyword):
        if (self.turn_details['stage'] == 'WAITING_FOR_PLAYER') and self.players[self.player_turn]['id'] == int(player_id):
            self.turn_details['stage'] = 'WAITING_FOR_SUBMISSIONS'
            self.turn_details['keyword'] = keyword
            self.turn_details['right-card'] = card
            self.turn_details['card-pairs'] = dict()
            self.turn_details['card-pairs'][card] = player_id
            self.turn_details['submitted'] = [card, ]

    def make_move_others(self, player_id, card):
        if (self.turn_details['stage'] == 'WAITING_FOR_SUBMISSIONS') and player_id not in self.turn_details['card-pairs'].values():
            self.turn_details['card-pairs'][card] = player_id
            self.turn_details['submitted'].append(card)
            if len(self.turn_details['submitted']) == self.player_limit:
                self.turn_details['votes'] = dict()
                self.turn_details['stage'] = 'WAITING_FOR_VOTES'
                return True
        return False

    def vote(self, player_id, card):
        if (self.turn_details['stage'] == 'WAITING_FOR_VOTES') and (
            self.turn_details['card-pairs'][card] != player_id) and (player_id not in self.turn_details['votes']):
            self.turn_details['votes'][player_id] = card
            if len(self.turn_details['votes']) == self.player_limit - 1:
                self.calc_scores()
                if self.turn_details['stage']=='GAME_OVER':
                    self.end_game()
                return True
        return False

    def get_index_by_id(self, id):
        n = next((index for (index, d) in enumerate(self.players) if d["id"] == int(id)), None)
        return n

    def set_next_cards(self):
        if(len(self.deck)<self.player_limit):
            return False
        for card, player in self.turn_details['card-pairs'].items():
            i = self.get_index_by_id(player)
            self.players[i]["hand"]=[e for e in self.players[i]["hand"] if e["id"]!=card]
            next_card = self.deck.pop()
            self.players[i]["hand"].append({ "id": next_card, "img": Card.objects.get(id=next_card).image.url})
        return True

    def get_payload(self):
        return {
            'stage' : self.turn_details['stage'],
            # 'players' : self.get_participants(),
            'details' : self.get_details(),
            'points' : self.get_scoreboard()
        }

    def get_voting_options(self):
        options = []
        self.turn_details['submitted'].sort()
        for c in self.turn_details['submitted']:
            options.append({
                'id': c,
                'img': "http://localhost:8000" + Card.objects.get(id=c).image.url
            })
        return options

    def get_details(self):
        stage = self.turn_details['stage']
        details = None
        if stage == 'WAITING_FOR_START':
            details = None
        elif stage == 'WAITING_FOR_PLAYER':
            details = {'turn': self.player_turn}
        elif stage == 'WAITING_FOR_SUBMISSIONS':
            details = {'submittedCount': len(self.turn_details['submitted']), 'keyword':self.turn_details['keyword'], 'turn': self.player_turn}
        elif stage == 'WAITING_FOR_VOTES':
            details = {'cards': self.get_voting_options(), 'keyword':self.turn_details['keyword'], 'turn': self.player_turn}
        elif stage == 'TURN_RESULTS_OVERVIEW':
            details = {'cardPairs':self.turn_details['card-pairs'],
                       'cards': self.get_voting_options(),
                       'votes' : self.turn_details['votes'],
                       'addedPoints':self.turn_details['added-points'],
                       'turn': self.player_turn}
        elif stage == 'GAME_OVER':
            details = {'winners': self.turn_details['winners'], 'turn': self.player_turn}
        return details

    def continueGame(self, player):
        if self.get_index_by_id(player)==self.player_turn:
            if (not self.set_next_cards()) or self.turn_details['stage']=='GAME_OVER':
                self.end_game()
            else:
                self.round_cleanup()

    def round_cleanup(self):
        self.player_turn = (self.player_turn+1)%self.player_limit
        self.turn_count+=1
        self.turn_details = {
            'stage': 'WAITING_FOR_PLAYER',
            'keyword': '',
            'right-card': -1,
            'card-pairs': dict(),
            'votes': dict(),
            'submitted': list(),
            'added-points': dict(),
            'winners' : list()
        }

    def end_game(self):
        self.has_ended = True
        self.turn_details = {
            'stage': 'GAME_OVER',
            'keyword': '',
            'right-card': -1,
            'card-pairs': dict(),
            'votes': dict(),
            'submitted': list(),
            'added-points': dict(),
            'winners' : list()
        }
        self.get_winner()
        self.set_achievements()

    def get_winner(self):
        self.turn_details['winners'] = []
        max = 0
        for player in self.players:
            player['won'] = False
            if player['score'] > max:
                max = player['score']
                self.turn_details['winners'] = [player]
            elif player['score'] == max:
                self.turn_details['winners'].append(player)
        for winner in self.turn_details['winners']:
            self.players[self.get_index_by_id(winner['id'])]['won']=True

    def set_achievements(self):
        for data in self.players:
            p = Player.objects.get(pk=data['id'])

            update_achievement(p, 'points', lambda v: v + data['score'])
            update_achievement(p, 'guess-streak', lambda v: max(v, data['longestStreak']))

            if data['won']:
                update_achievement(p, 'wins', lambda v: v + 1)
                update_achievement(p, 'win-speed', lambda v: min(v, self.turn_count), def_value=sys.maxsize)

                others_max_points = max(d['score'] for d in self.players if d != data)
                difference = data['score'] - others_max_points
                update_achievement(p, 'win-difference', lambda v: max(v, difference))

    def get_scoreboard(self):
        scoreboard = dict()
        for i in range(0, len(self.players)):
            scoreboard[self.players[i]['id']] = {
                "score":self.players[i]['score'],
                "streak":self.players[i]['streak']
            }
        return scoreboard

    def calc_scores(self):
        self.turn_details['stage'] = 'TURN_RESULTS_OVERVIEW'
        addedPoints = {}
        # answerSum - cardId:votes
        answerSum = dict()
        for vote in self.turn_details['votes'].values():
            if vote in answerSum.keys():
                answerSum[vote] += 1
            else:
                answerSum[vote] = 1

        addedPoints = [0 for i in range(0,self.player_limit)]

        # give points to player whose turn it is
        if self.turn_details['right-card'] in answerSum.keys() and answerSum[
            self.turn_details['right-card']] < self.player_limit - 1:
            self.players[self.player_turn]['score'] += 3
            addedPoints[self.player_turn] = 3

            for i in range(0, self.player_limit):
                if i == self.player_turn:
                    continue

                id = str(self.players[i]['id'])
                voted_for_card = self.turn_details['votes'][id]

                if voted_for_card == self.turn_details['right-card']:
                    self.players[i]['score'] += 3
                    self.players[i]['streak'] += 1
                    if self.players[i]['longestStreak'] < self.players[i]['streak']:
                        self.players[i]['longestStreak'] = self.players[i]['streak']
                    addedPoints[i] += 3
                else:
                    voted_for_player = self.turn_details['card-pairs'][voted_for_card]
                    self.players[self.get_index_by_id(voted_for_player)]['score'] +=1
                    addedPoints[self.get_index_by_id(voted_for_player)] += 1
                    if self.players[i]['longestStreak'] < self.players[i]['streak']:
                        self.players[i]['longestStreak'] = self.players[i]['streak']
                    self.players[i]['streak'] = 0

        else:
            for i in range(0, self.player_limit):
                if i == self.player_turn:
                    continue
                self.players[i]['score'] += 2
                addedPoints[i] += 2
                id = str(self.players[i]['id'])
                voted_for_card = self.turn_details['votes'][id]
                if voted_for_card == self.turn_details['right-card']:
                    self.players[i]['streak'] += 1
                    if self.players[i]['longestStreak'] < self.players[i]['streak']:
                        self.players[i]['longestStreak'] = self.players[i]['streak']
                else:
                    voted_for_player = self.turn_details['card-pairs'][voted_for_card]
                    self.players[self.get_index_by_id(voted_for_player)]['score'] +=1
                    addedPoints[self.get_index_by_id(voted_for_player)] += 1
                    if self.players[i]['longestStreak'] < self.players[i]['streak']:
                        self.players[i]['longestStreak'] = self.players[i]['streak']
                    self.players[i]['streak'] = 0

        for player in self.players:
            if player['score']>=30:
                self.turn_details['stage'] = 'GAME_OVER'

        self.turn_details['added-points'] = addedPoints

