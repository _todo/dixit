import inspect

from django.contrib import admin
from django.db.models import Model

from . import models


def is_app_model(m):
    return (inspect.isclass(m)
            and issubclass(m, Model)
            and m.__module__ == models.__name__
            and not m._meta.abstract)


# Register all non-abstract models defined in this app's models.py.
# inspect.getmembers returns (name, model) tuples.
admin.site.register(m[1] for m in inspect.getmembers(models, is_app_model))
