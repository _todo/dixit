from rest_framework import serializers

from . import models


class AvatarSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Avatar
        fields = ('image', 'codename')


class PlayerSerializer(serializers.HyperlinkedModelSerializer):
    avatar = AvatarSerializer()
    class Meta:
        model = models.Player
        fields = ('id', 'username', 'profile_picture', 'avatar')


class CardSerializer(serializers.HyperlinkedModelSerializer):
    class Meta:
        model = models.Card
        fields = ('id', 'image', 'codename')  # TODO: expansion?
