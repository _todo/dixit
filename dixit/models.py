from django.contrib.auth.models import AbstractUser, User
from django.db import models


class NameMixin(models.Model):
    name = models.CharField(max_length=255)

    class Meta:
        abstract = True


class CodenameMixin(models.Model):
    codename = models.CharField(max_length=100, unique=True)

    class Meta:
        abstract = True

    def __str__(self):
        return self.codename


# Players

class Avatar(CodenameMixin):
    DEFAULT_ID = 1
    image = models.ImageField(upload_to='avatars')


class Player(AbstractUser):
    avatar = models.ForeignKey(
        Avatar, on_delete=models.SET_DEFAULT, default=Avatar.DEFAULT_ID
    )
    profile_picture = models.ImageField(
        upload_to='profile_pictures', default='profile_pictures/default.png'
    )
    email_confirmed = models.BooleanField(default=False)
    is_vip = models.BooleanField(default=False)
    vip_expiry = models.DateTimeField(null=True)

    @property
    def is_admin(self):
        return self.is_staff

    @is_admin.setter
    def is_admin(self, value):
        self.is_staff = value


# Achievements

class AchievementType(NameMixin, CodenameMixin):
    pass


class Achievement(models.Model):
    player = models.ForeignKey(
        Player, on_delete=models.CASCADE, related_name='achievements'
    )
    achievement_type = models.ForeignKey(
        AchievementType, on_delete=models.CASCADE, related_name='achievements'
    )
    value = models.FloatField()

    class Meta:
        unique_together = ('player', 'achievement_type')


# Cards

class Expansion(NameMixin, CodenameMixin):
    image = models.ImageField(
        upload_to='expansions', default='expansions/default.png'
    )


class Card(CodenameMixin):
    expansion = models.ForeignKey(
        Expansion, on_delete=models.SET_NULL, related_name='cards', null=True
    )
    image = models.ImageField(upload_to='cards')


# Game settings

class Constant(NameMixin):
    value = models.FloatField()


class Color(Constant):
    pass
