from django.core.management import call_command
from django.core.serializers import base, python
from django.db import migrations


def load_fixture(apps, schema_editor):
    old_get_model = python._get_model

    def _get_model(model_identifier):
        try:
            return apps.get_model(model_identifier)
        except (LookupError, TypeError):
            raise base.DeserializationError(
                f"Invalid model identifier: {model_identifier}")

    python._get_model = _get_model
    try:
        call_command('loaddata', 'initial_data', app_label='dixit')
    finally:
        python._get_model = old_get_model


class Migration(migrations.Migration):
    dependencies = [
        ('dixit', '0002_auto_20180608_0726'),
    ]

    operations = [
        migrations.RunPython(load_fixture),
    ]
