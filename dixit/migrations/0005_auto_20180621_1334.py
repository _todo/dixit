# Generated by Django 2.0.5 on 2018-06-21 11:34

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('dixit', '0004_player_email_confirmed'),
    ]

    operations = [
        migrations.AlterField(
            model_name='achievement',
            name='achievement_type',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='achievements', to='dixit.AchievementType'),
        ),
        migrations.AlterField(
            model_name='achievement',
            name='player',
            field=models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='achievements', to=settings.AUTH_USER_MODEL),
        ),
        migrations.AlterField(
            model_name='card',
            name='expansion',
            field=models.ForeignKey(null=True, on_delete=django.db.models.deletion.SET_NULL, related_name='cards', to='dixit.Expansion'),
        ),
    ]
