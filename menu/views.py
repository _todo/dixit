from django.shortcuts import render

from dixit import __meta__


def home(request):
    return render(request, 'menu/home.html', {
        'version': __meta__.__version__,
        'author': __meta__.__author__,
        'team': __meta__.__team__,
        'copyright': __meta__.__copyright__,
    })
