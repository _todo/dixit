from setuptools import find_packages, setup


meta = {}
with open('dixit/__meta__.py') as f:
    exec(f.read(), meta)


with open('requirements.txt') as f:
    lines = filter(None, map(str.strip, f))
    install_requires = [req for req in lines if req[0] not in '-#']


setup(
    name=meta['__name__'],
    version=meta['__version__'],
    author=meta['__author__'],
    url=meta['__url__'],
    description=meta['__description__'],
    packages=find_packages(),
    install_requires=install_requires,
    scripts=['manage.py'],
)
