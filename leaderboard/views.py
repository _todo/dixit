from django.shortcuts import render

from dixit.models import Achievement, AchievementType


def leaderboard(request):
    ordered_points = Achievement.objects.select_related('player').filter(
        achievement_type__codename='points').order_by('-value')

    players_by_points = (a.player for a in ordered_points)

    types = AchievementType.objects.all()
    table = {
        p: [p.achievements.filter(achievement_type=t).first()  # object or None
            for t in types]
        for p in players_by_points
    }

    return render(request, 'leaderboard/leaderboard.html', {
        'types': types,
        'table': table,
    })
