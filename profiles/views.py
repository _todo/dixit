from datetime import timedelta, datetime

from django.contrib.auth import get_user
from django.contrib.auth.decorators import login_required
from django.shortcuts import redirect, render

from dixit.models import Player
from profiles.forms import EditProfileForm, EditVIPProfileForm , BecomeVipForm


@login_required
def profile(request):
    return render(request, 'profiles/profile.html', {'user': request.user})


@login_required
def profile_edit(request):
    if request.user.is_vip or request.user.is_admin:
        form_type = EditVIPProfileForm
    else:
        form_type = EditProfileForm

    if request.method == 'POST':
        form = form_type(data=request.POST, instance=request.user)
        if form.is_valid():
            request.user = form.save()

            if 'profile_picture' in request.FILES:
                request.user.profile_picture = request.FILES['profile_picture']

            request.user.save()
            return redirect('profile')

    form = form_type(instance=request.user)
    return render(request, 'profiles/profile_edit.html', {'form': form})


@login_required
def become_vip(request):
    message = ''
    user = get_user(request)
    player = Player.objects.get(pk=user.id)
    if player.is_vip:
        return redirect(profile)
    if request.method == 'POST':
        form = BecomeVipForm(request.POST)
        if form.is_valid():
            vip_code = form.cleaned_data['vip_code']
            if vip_code == '1234':
                player.is_vip = True
                player.vip_expiry = datetime.now() + timedelta(days=30)
                player.save()
                return redirect(profile)
            else:
                message = 'Wrong VIP code'
    form = BecomeVipForm()
    return render(request, 'profiles/become_vip.html', {'form': form, 'message': message})
