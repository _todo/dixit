from django import forms
from django.contrib.auth.forms import UserChangeForm

from dixit.models import Player


class EditProfileForm(UserChangeForm):
    class Meta:
        model = Player
        fields = (
            'username',
            'first_name',
            'last_name',
            'email',
            'password',
            'profile_picture',
        )


class BecomeVipForm(forms.Form):
    vip_code = forms.CharField(label='Enter the code you received:', max_length=100)


class EditVIPProfileForm(UserChangeForm):
    class Meta:
        model = EditProfileForm.Meta.model
        fields = EditProfileForm.Meta.fields + ('avatar',)
