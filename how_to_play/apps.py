from django.apps import AppConfig


class HowToPlayConfig(AppConfig):
    name = 'how_to_play'
