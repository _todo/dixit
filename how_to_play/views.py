from django.shortcuts import render


def how_to_play(request):
    return render(request, 'how_to_play/how_to_play.html')
