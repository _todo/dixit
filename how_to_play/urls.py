from django.urls import path

from . import views

urlpatterns = [
    path('how_to_play', views.how_to_play, name='how_to_play')
]
